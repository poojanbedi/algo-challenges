function findSumOperands(arr, sum) {
  var remainders = {};
  for(var i = 0, len = arr.length; i < len; i += 1) {
    var _current = arr[i];
    if(sum >= _current) {
      if( _current < 0 ) { // Means negative vallue
        _current = Math.abs(_current);
      }
      var balance = sum - _current;
      if(remainders.hasOwnProperty(balance)) {
        return [ remainders[balance],  arr[i]];
      } else {
        remainders[_current] = arr[i];
      }
    }
  }
  throw new Error('No Operands found!');
}
console.log(findSumOperands( [-1, 5, 8,  12, -14, 10], 15 )); // [-1, -14]
console.log(findSumOperands( [-1, 5, 8, 10, 12, -14], 15 )); // [5, 10]
console.log(findSumOperands( [0, -1, 6, 8, 12, 15, -14], 15 )); // [0, 15]
console.log(findSumOperands( [0, -1, 6, 8, 12, 15, -14], 25 )); // Error:: No Operands found!