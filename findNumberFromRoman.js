/**
 * Create a function that takes a Roman numeral as its argument and returns its value as a numeric decimal integer. You don't need to validate the form of the Roman numeral.
 *
 * Modern Roman numerals are written by expressing each decimal digit of the number to be encoded separately, starting with the leftmost digit and skipping any 0s. So 1990 is rendered "MCMXC" (1000 = M, 900 = CM, 90 = XC) and 2008 is rendered "MMVIII" (2000 = MM, 8 = VIII). The Roman numeral for 1666, "MDCLXVI", uses each letter in descending order.
 *
 * Assuming it till 4999 or MMMMCMXCIX as roman number
 */
function romanToNumber(romanNumber = "") {
  const romanNumberIndex = {
    "I": 1,
    "V": 5,
    "X": 10,
    "L": 50,
    "C": 100,
    "D": 500,
    "M": 1000
  };

  let romanNumberArr = romanNumber.toUpperCase().split('');
  for (const [index,romanNumber] of romanNumberArr.entries()) {
    if(romanNumberIndex.hasOwnProperty(romanNumber)) {
      romanNumberArr[index] = romanNumberIndex[romanNumber];
    } else {
      throw new Error("Invalid Roman Number");
    }
  }

  let finalSum = 0;
  for (let index = 0; index < romanNumberArr.length; index += 1) {
    let currentVal = romanNumberArr[index];
    let nextValue = romanNumberArr[index + 1];
    if(currentVal < nextValue) {
      currentVal = nextValue - currentVal;
      romanNumberArr.splice(index, 1);
    }
    finalSum += currentVal;
  }

  return finalSum;

}
console.log(romanToNumber("XXI")); //21

console.log(romanToNumber("MMMMCMXCIX")); // 4999

console.log(romanToNumber("MMCDLXIV")); // 2464


/*
  Test Driven Development.
describe("Solution", function(){
  it("should return 21", function(){
    Test.assertEquals(romanToNumber("XXI"), 21, "21 was expected");
  });
  
  it("should return 4999", function(){
    Test.assertEquals(romanToNumber("MMMMCMXCIX"), 4999, "4999 was expected");
  });
  
  it("should return 2464", function(){
    Test.assertEquals(romanToNumber("MMCDLXIV"), 2464, "2464 was expected");
  });
});*/