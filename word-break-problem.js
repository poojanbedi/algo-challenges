/** Problem :: get first 100 character long string from a big message but dont cut the last word (word break problem) 
 * Given below string
 * Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
 ***/

function findFirst100Chars(str) {
  let strLength = str.length;
  let strIndex = 0;
  let numberOfChar = 0;
  let returnStr = '';
  if(str.length <= 100) {
    return str;
  } 

  for(;strIndex < strLength; strIndex += 1) {
    if(str[strIndex] !== ' ' && str[strIndex] !== '.' && str[strIndex] !== ',' && str[strIndex] !== '?' && str[strIndex] !== "'" && str[strIndex] !== '"' && str[strIndex] !== "!"){
      numberOfChar += 1;
    }
    returnStr += str[strIndex];
    if(numberOfChar >= 100) {
      while(str[strIndex] !== ' ' && str[strIndex] !== '.' && str[strIndex] !== ',' && str[strIndex] !== '?' && str[strIndex] !== "!") {
        strIndex += 1;
        returnStr += str[strIndex];
      }
      break;
    }
  }
  return returnStr;
}

console.log(findFirst100Chars("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."));