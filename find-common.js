/** Problem Statement:: From two sorted array how would you find common number? */

function findCommon(arr1, arr2) {
  let commonValues = {};
  let index1 = 0;
  let index2 = 0;
  let arr1Len = arr1.length;
  let arr2Len = arr2.length;
  for(; index1 < arr1Len; index1 +=1 ) {
    for(; index2 < arr2Len; index2 += 1 ){
      if(arr2[index2] >= arr1[index1]) {
        if(arr2[index2] === arr1[index1]) {
          commonValues[arr1[index1]] = true;
        }
        break;
      }
    }
  }
  return commonValues;
}

let customSort = (a, b) => { return a-b; }
let _arr1 = [ 4,2,5,7,9,0,23,-87,34,123,5,77,22 ].sort(customSort);
let _arr2 = [ 5,6,2,6,9,3,22,35,78,21,878,12,-87 ].sort(customSort);
console.log(findCommon(_arr1, _arr2));
